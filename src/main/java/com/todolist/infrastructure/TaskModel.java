//package com.todolist.infrastructure;
//
//import org.hibernate.annotations.CreationTimestamp;
//
//import javax.persistence.*;
//import java.time.LocalDateTime;
//
//
//@Entity
//public class TaskModel {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//
//    @Column(nullable = false)
//    private String title;
//
//    @Column(columnDefinition = "TEXT", nullable = false)
//    private String description;
//
//    @CreationTimestamp
//    @Column(name = "created_at", nullable = false, updatable = false)
//    private LocalDateTime createdAt;
//
//    @CreationTimestamp
//    @Column(name = "updated_at", nullable = false)
//    private LocalDateTime updatedAt;
//
//    @Column(nullable = false)
//    private Priority priority;
//
//    @Column(nullable = false)
//    private TaskStatus taskStatus;
//
//    public TaskModel() {
//    }
//
//    public TaskModel(
//            Long id,
//            String title,
//            String description,
//            LocalDateTime createdAt,
//            LocalDateTime updatedAt,
//            Priority priority,
//            TaskStatus taskStatus) {
//        this.id = id;
//        this.title = title;
//        this.description = description;
//        this.createdAt = createdAt;
//        this.updatedAt = updatedAt;
//        this.priority = priority;
//        this.taskStatus = taskStatus;
//    }
//
//    public TaskModel(String title, String description, Priority priority, TaskStatus taskStatus) {
//        this.title = title;
//        this.description = description;
//        this.priority = priority;
//        this.taskStatus = taskStatus;
//    }
//
//    public static TaskModel create(
//            String title, String description, Priority priority, TaskStatus taskStatus) {
//        return new TaskModel(title, description, priority, taskStatus);
//    }
//
//    public void update(String title, String description, Priority priority) {
//        this.title = title;
//        this.description = description;
//        this.priority = priority;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public LocalDateTime getCreatedAt() {
//        return createdAt;
//    }
//
//    public void setCreatedAt(LocalDateTime createdAt) {
//        this.createdAt = createdAt;
//    }
//
//    public LocalDateTime getUpdatedAt() {
//        return updatedAt;
//    }
//
//    public void setUpdatedAt(LocalDateTime updatedAt) {
//        this.updatedAt = updatedAt;
//    }
//
//    public Priority getPriority() {
//        return priority;
//    }
//
//    public void setPriority(Priority priority) {
//        this.priority = priority;
//    }
//
//    public TaskStatus getTaskStatus() {
//        return taskStatus;
//    }
//
//    public void setTaskStatus(TaskStatus taskStatus) {
//        this.taskStatus = taskStatus;
//    }
//}
